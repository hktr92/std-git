<?php
declare(strict_types=1);

namespace Std\Git;

use Exception;
use Oopize\Util\ArrayUtil;
use Oopize\Util\ClassUtil;
use Oopize\Util\VarUtil;
use ReflectionException;
use const PHP_EOL;

/**
 * Class GitException
 * @package Std\Git
 */
final class GitException extends Exception {
    /**
     * @param $arrayOrString
     *
     * @return string
     * @throws ReflectionException
     */
    private static function arrayToString(/*php8:array|string */ $arrayOrString): string {
        if (ClassUtil::isInstanceOf($arrayOrString, ArrayUtil::class)) {
            $arrayOrString = $arrayOrString->toArray();
        }

        if (VarUtil::isString($arrayOrString)) {
            return $arrayOrString;
        }

        return (new ArrayUtil($arrayOrString))->toString(PHP_EOL);
    }

    /**
     * @param string $repository
     *
     * @return GitException
     */
    public static function repositoryNotFound(string $repository): GitException {
        return new self("[Std\Git] Repository '{$repository}' not found. Did you forget to 'git init'?", 0, null);
    }

    /**
     * @param string    $gitModule
     * @param ArrayUtil $output
     *
     * @return GitException
     * @throws ReflectionException
     */
    public static function commandFailed(string $gitModule, ArrayUtil $output): GitException {
        $message = $gitModule;
        $message .= PHP_EOL.'Command output: '.PHP_EOL.static::arrayToString($output);

        return new self($message);
    }
}
