<?php
declare(strict_types=1);

namespace Std\Git;

use Oopize\Util\ArrayUtil;
use Oopize\Util\DirUtil;
use Oopize\Util\RegexUtil;
use Oopize\Util\StringUtil;
use Oopize\Util\SysUtil;
use Oopize\Util\VarUtil;
use ReflectionException;
use RuntimeException;
use const PHP_OS;

/**
 * Class Git
 *
 * Git is a class that fetches common information about a given repository,
 * such as current branch, checkout capability, latest tag, commit history
 * and fetch remote updates.
 *
 * It does not cover every functionality of native git, it just comes into
 * completion to it, in order to use those information in PHP world.
 *
 * @package Std\Git
 * @since   1.0.0
 */
final class Git {
    /**
     * @var string
     */
    private $repository = '';

    /**
     * @var string|null
     * @internal
     */
    private $cwd = null;

    /**
     * @var string
     */
    private $gitExec;

    /**
     * Git constructor.
     *
     * @param string|null $repository
     *
     * @throws GitException
     */
    public function __construct(?string $repository = null) {
        if (null !== $repository) {
            $this->setLocalRepository($repository);
        }
    }

    /**
     * @param string $repository
     *
     * @throws GitException
     */
    public function setLocalRepository(string $repository): void {
        $this->gitExec = '/usr/bin/git';

        if (false === DirUtil::isValid($repository)) {
            throw GitException::repositoryNotFound($repository);
        }

        if ('.git' === DirUtil::getBaseDirectory($repository)) {
            $repository = DirUtil::getDirectoryName($repository);
        }

        $this->repository = DirUtil::getRealPath($repository);
    }

    /**
     * Returns the Git executable path
     *
     * @return string
     */
    public function getGitExec(): string {
        return $this->gitExec;
    }

    /**
     * Sets the Git executable path
     *
     * @param string $gitExec
     */
    public function setGitExec(string $gitExec): void {
        $this->gitExec = $gitExec;
    }

    /**
     * @param string $branch
     *
     * @throws GitException
     * @throws ReflectionException
     * @deprecated in favor of {@see \Std\Git\Git::setBranch()}
     */
    public function branch(string $branch): void {
        $this->setBranch($branch);
    }

    /**
     * Switches the current branch / tag / commit ID
     *
     * @param string $branch
     *
     * @throws GitException
     * @throws ReflectionException
     */
    public function setBranch(string $branch): void {
        $this->extractFromCommand('checkout', [$branch]);
    }

    /**
     * Fetches and pulls the updates from Git repository.
     *
     * If git pull fails, then it tries to reset to origin/master with --hard flag on.
     *
     * @return array
     * @throws GitException
     * @throws ReflectionException
     */
    public function getUpdates(): array {
        $output   = [];
        $output[] = $this->extractFromCommand('fetch', ['--all']);
        try {
            $output[] = $this->extractFromCommand('pull', ['-p']);
        } catch (GitException $e) {
            $output[] = $this->extractFromCommand('reset origin/master', ['--hard']);
        }

        return $output;
    }

    /**
     * @return string
     * @throws GitException
     * @throws ReflectionException
     * @deprecated in favor of {@see \Std\Git\Git::getCurrentBranch()}.
     */
    public function getBranch(): string {
        return $this->getCurrentBranch() ?? 'N/A';
    }

    /**
     * Gets the current branch version.
     *
     * @return string
     * @throws GitException
     * @throws ReflectionException
     */
    public function getCurrentBranch(): ?string {
        $branch = $this->extractFromCommand(
            'branch',
            ['-a'],
            false,
            function ($value) {
                if (isset($value[0]) && $value[0] === '*') {
                    return StringUtil::trim(StringUtil::cut($value, 1));
                }

                return false;
            }
        );

        if (ArrayUtil::isArray($branch)) {
            return $branch[0];
        }

        return null;
    }

    /**
     * Gets the commit ID.
     *
     * @param bool $short
     *
     * @return string
     * @throws GitException
     * @throws ReflectionException
     */
    public function getCommitID(bool $short = false): ?string {
        $commitID = $this->extractFromCommand(
            'log',
            [
                'pretty' => 'format:"%H"',
                'n'      => 1,
            ],
            true
        );

        if (RegexUtil::match($commitID, '/^[0-9a-f]{40}$/i')) {
            if ($short) {
                return StringUtil::cut($commitID, 0, 7);
            }

            return $commitID;
        }

        return null;
    }

    /**
     * Gets the latest Git tag release.
     *
     * @return string
     * @throws GitException
     * @throws ReflectionException
     */
    public function getLatestRelease(): string {
        $versions = $this->extractFromCommand('tag');
        if (null === $versions) {
            return 'N/A';
        }

        $versions = $versions->sort('version_compare');

        return $versions->last();
    }

    /**
     * Fetches the Git output.
     *
     * @param string $gitModule
     * @param array  $moduleArgs
     * @param bool   $execOutput
     * @param string $filter
     *
     * @return array|string|null|ArrayUtil
     * @throws GitException
     * @throws ReflectionException
     */
    protected function extractFromCommand(
        string $gitModule,
        array $moduleArgs = [],
        bool $execOutput = false,
        $filter = 'trim'
    ) {
        if (StringUtil::startsWith(PHP_OS, 'WIN')) {
            throw new RuntimeException("Incompatible platform: ".PHP_OS);
        }

        $output   = new ArrayUtil;
        $exitCode = null;

        $args = new ArrayUtil;
        if (!empty($moduleArgs)) {
            if (!isset($moduleArgs[0])) {
                foreach ($moduleArgs as $arg => $val) {
                    $isShortOpt = StringUtil::length($arg) === 1;

                    $dash = $isShortOpt
                        ? '-'
                        : '--';

                    if (null === $val) {
                        $sep = '';
                    } else {
                        $sep = $isShortOpt
                            ? ' '
                            : '=';
                    }

                    $args[] = "{$dash}{$arg}{$sep}{$val}";
                }
            } else {
                $args = new ArrayUtil($moduleArgs);
            }
        }

        $args = $args->toString(' ');

        $this->begin();
        $_execOut = SysUtil::execute("{$this->getGitExec()} {$gitModule} {$args} 2>&1");
        $lastLine = $_execOut['lastLine'];
        $output   = $_execOut['output'];
        $exitCode = $_execOut['exitCode'];
        $this->end();

        if ($exitCode !== 0 || !ArrayUtil::isArray($output)) {
            throw GitException::commandFailed($gitModule, $output);
        }

        $newArray = new ArrayUtil;

        foreach ($output as $line) {
            $value = $filter($line);

            if ($value === false) {
                continue;
            }

            $newArray[] = $value;
        }

        $output = $newArray;

        // empty array
        if (!isset($output[0])) {
            return null;
        }

        if ($execOutput) {
            return $lastLine;
        }

        return $output;
    }

    /**
     *
     */
    protected function begin(): void {
        if (null === $this->cwd) {
            $this->cwd = SysUtil::getCurrentWorkingDir();
            SysUtil::changeDirectory($this->repository);
        }
    }

    /**
     *
     */
    protected function end(): void {
        if (VarUtil::isString($this->cwd)) {
            SysUtil::changeDirectory($this->cwd);
        }

        $this->cwd = null;
    }
}
